package me.nfs;

import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class ImageLabel extends JLabel implements Runnable{

	private String filename;
	private ImageIcon icon;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4625134163988940569L;

	public ImageLabel(String filename) {
		this.filename = filename;
		URL url = this.getClass().getResource(filename);
		icon = new ImageIcon(url);
		setIcon(icon);
	}
	
	@Override
	public void run() {
		boolean running = true;
		while (running) {
			try {
				if (isDisplayable()) {
					icon.paintIcon(this, this.getGraphics(), 0, 0);
				}
				Thread.sleep(1000);
			} catch (InterruptedException ex){
				running = false;
			}
		} 
	}

	public void init() {
		Thread t = new Thread(this);
		t.start();
	}
}
