package me.nfs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;

public class GamePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3109791881557462263L;
	private LeftPanel lpanel;
	private Model model;
	private JSlider sliderTG;
	private JSlider sliderLR;
	private ImageLabel lblGIF;
	private ImageLabel lblGIF1;
	private ImageLabel lblGIF2;
	private JLabel lblWin;
	private Timer t;

	/**
	 * Create the panel.
	 */
	public GamePanel(LeftPanel lp) {
		lpanel = lp;
		t = new Timer();
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel label = new JLabel("Тормоз");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.anchor = GridBagConstraints.EAST;
		gbc_label.gridx = 0;
		gbc_label.gridy = 1;
		panel.add(label, gbc_label);
		
		sliderTG = new JSlider();
		GridBagConstraints gbc_sliderTG = new GridBagConstraints();
		gbc_sliderTG.insets = new Insets(0, 0, 5, 5);
		gbc_sliderTG.fill = GridBagConstraints.HORIZONTAL;
		gbc_sliderTG.gridx = 1;
		gbc_sliderTG.gridy = 1;
		gbc_sliderTG.weightx = 0.1;
		panel.add(sliderTG, gbc_sliderTG);
		sliderTG.setPaintTicks(true);
		sliderTG.setMajorTickSpacing(10);
		
		JLabel label_1 = new JLabel("Газ");
		label_1.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 0);
		gbc_label_1.gridx = 2;
		gbc_label_1.gridy = 1;
		panel.add(label_1, gbc_label_1);
		
		JLabel label_2 = new JLabel("Налево");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.anchor = GridBagConstraints.EAST;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.gridx = 0;
		gbc_label_2.gridy = 3;
		panel.add(label_2, gbc_label_2);
		
		sliderLR = new JSlider();
		GridBagConstraints gbc_slider_1 = new GridBagConstraints();
		gbc_slider_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_slider_1.insets = new Insets(0, 0, 5, 5);
		gbc_slider_1.gridx = 1;
		gbc_slider_1.gridy = 3;
		gbc_slider_1.weightx = 0.1;
		panel.add(sliderLR, gbc_slider_1);
		sliderLR.setPaintTicks(true);
		sliderLR.setMajorTickSpacing(10);
		
		JLabel label_3 = new JLabel("Направо");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.insets = new Insets(0, 0, 5, 0);
		gbc_label_3.anchor = GridBagConstraints.WEST;
		gbc_label_3.gridx = 2;
		gbc_label_3.gridy = 3;
		panel.add(label_3, gbc_label_3);
		
		JLabel label_4 = new JLabel("Ход:");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.anchor = GridBagConstraints.EAST;
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 0;
		gbc_label_4.gridy = 5;
		panel.add(label_4, gbc_label_4);
		
		final JLabel lblPlayer = new JLabel("Player 1 (green)");
		GridBagConstraints gbc_lblPlayer = new GridBagConstraints();
		gbc_lblPlayer.anchor = GridBagConstraints.WEST;
		gbc_lblPlayer.insets = new Insets(0, 0, 5, 5);
		gbc_lblPlayer.gridx = 1;
		gbc_lblPlayer.gridy = 5;
		panel.add(lblPlayer, gbc_lblPlayer);
		
		JLabel lblVx = new JLabel("vx:");
		GridBagConstraints gbc_lblVx = new GridBagConstraints();
		gbc_lblVx.anchor = GridBagConstraints.EAST;
		gbc_lblVx.insets = new Insets(0, 0, 5, 5);
		gbc_lblVx.gridx = 0;
		gbc_lblVx.gridy = 6;
		panel.add(lblVx, gbc_lblVx);
		
		final JLabel label_5 = new JLabel("0");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.anchor = GridBagConstraints.WEST;
		gbc_label_5.insets = new Insets(0, 0, 5, 5);
		gbc_label_5.gridx = 1;
		gbc_label_5.gridy = 6;
		panel.add(label_5, gbc_label_5);
		
		JLabel lblVy = new JLabel("vy:");
		GridBagConstraints gbc_lblVy = new GridBagConstraints();
		gbc_lblVy.anchor = GridBagConstraints.EAST;
		gbc_lblVy.insets = new Insets(0, 0, 5, 5);
		gbc_lblVy.gridx = 0;
		gbc_lblVy.gridy = 7;
		panel.add(lblVy, gbc_lblVy);
		
		final JLabel label_6 = new JLabel("0");
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.anchor = GridBagConstraints.WEST;
		gbc_label_6.insets = new Insets(0, 0, 5, 5);
		gbc_label_6.gridx = 1;
		gbc_label_6.gridy = 7;
		panel.add(label_6, gbc_label_6);
		
		JButton btnGo = new JButton("GO");
		GridBagConstraints gbc_btnGo = new GridBagConstraints();
		gbc_btnGo.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnGo.insets = new Insets(0, 0, 5, 5);
		gbc_btnGo.gridx = 1;
		gbc_btnGo.gridy = 10;
		panel.add(btnGo, gbc_btnGo);
		btnGo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!model.isFinished() && !model.isWaiting()) {
					setMoveVisible(true);
					try {
						t.cancel();
					} catch (IllegalStateException ex) {
					}
					t = new Timer();
					t.schedule(new TimerTask() {
						@Override
						public void run() {
							setMoveVisible(false);
						}
					}, 10000);
					int vx = (int) Math.round(((double)(sliderTG.getValue() - 50)) / 10);
					int vy = (int) Math.round(((double)(sliderLR.getValue() - 50)) / 10);
					sliderTG.setValue(50);
					sliderLR.setValue(50);
					
					model.move(vx, vy);
					
					String pcolor = "";
					switch (model.getPlayerNUM()) {
					case 1: pcolor = "(green)"; break;
					case 2: pcolor = "(red)"; break;
					}
					lblPlayer.setText("Player "+model.getPlayerNUM()+" "+pcolor);
					label_5.setText(""+model.getPlayerVX());
					label_6.setText(""+model.getPlayerVY());
				}
			}
		});
		
		lblGIF = new ImageLabel("resources/move.gif");
		lblGIF.setSize(300, 300);
		lblGIF.setVisible(false);
		GridBagConstraints gbc_lblGIF = new GridBagConstraints();
		gbc_lblGIF.insets = new Insets(0, 0, 5, 0);
		gbc_lblGIF.gridx = 0;
		gbc_lblGIF.gridy = 5;
		add(lblGIF, gbc_lblGIF);
		
		lblGIF1 = new ImageLabel("resources/break.gif");
		lblGIF1.setSize(300, 300);
		lblGIF1.setVisible(false);
		GridBagConstraints gbc_lblGIF1 = new GridBagConstraints();
		gbc_lblGIF1.insets = new Insets(0, 0, 5, 0);
		gbc_lblGIF1.gridx = 0;
		gbc_lblGIF1.gridy = 6;
		add(lblGIF1, gbc_lblGIF1);
		
		lblWin = new JLabel("");
		lblWin.setVisible(false);
		GridBagConstraints gbc_lblWin = new GridBagConstraints();
		gbc_lblWin.insets = new Insets(0, 0, 5, 0);
		gbc_lblWin.gridx = 0;
		gbc_lblWin.gridy = 4;
		gbc_lblWin.weighty = 1;
		add(lblWin, gbc_lblWin);
		
		lblGIF2 = new ImageLabel("resources/win.gif");
		lblGIF2.setSize(300, 300);
		lblGIF2.setVisible(false);
		GridBagConstraints gbc_lblGIF2 = new GridBagConstraints();
		gbc_lblGIF2.insets = new Insets(0, 0, 5, 0);
		gbc_lblGIF2.gridx = 0;
		gbc_lblGIF2.gridy = 7;
		add(lblGIF2, gbc_lblGIF2);
		
		JButton btnConfigure = new JButton("Configure");
		GridBagConstraints gbc_btnConfigure = new GridBagConstraints();
		gbc_btnConfigure.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnConfigure.gridx = 0;
		gbc_btnConfigure.gridy = 13;
		add(btnConfigure, gbc_btnConfigure);
		btnConfigure.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				disableGIF();
				lblPlayer.setText("Player 1 (green)");
				label_5.setText("0");
				label_6.setText("0");
				lpanel.switchMode();
			}
		});
	}
	
	public void setModel(Model m) {
		model = m;
	}
	
	public void disableGIF() {
		lblWin.setVisible(false);
		lblGIF.setVisible(false);
		lblGIF1.setVisible(false);
		lblGIF2.setVisible(false);
	}
	
	public void setMoveVisible(boolean b) {
		if(b) {
			if (lblGIF1.isVisible())
				lblGIF1.setVisible(false);
			if (lblGIF2.isVisible())
				lblGIF2.setVisible(false);
		}
		lblGIF.setVisible(b);
	}
	
	public void setBreakVisible(boolean b, int num) {
		if(b) {
			if (lblGIF.isVisible())
				lblGIF.setVisible(false);
			if (lblGIF2.isVisible())
				lblGIF2.setVisible(false);
		}
		lblWin.setText("Player "+num+" died.");
		lblWin.setVisible(true);
		lblGIF1.setVisible(b);
	}
	
	public void setWinVisible(boolean b, int num) {
		if (b) {
			if (lblGIF.isVisible())
				lblGIF.setVisible(false);
			if (lblGIF1.isVisible())
				lblGIF1.setVisible(false);
		}
		lblWin.setText("Player "+num+" victory!");
		lblWin.setVisible(true);
		lblGIF2.setVisible(b);
	}
}
