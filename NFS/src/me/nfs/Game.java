package me.nfs;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;
import java.awt.BorderLayout;

import javax.swing.JLabel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;

public class Game extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2776272559265219968L;
	private JPanel contentPane;
	private LeftPanel leftPanel;
	private RightPanel rightPanel;
	private Thread rt;
	private ImageMouseListener listener;
	private Model model;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Game frame = new Game();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Game() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Need For Speed v0.0000000025");
		Dimension s_size = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0, 0, s_size.width, s_size.height);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setPreferredSize(new Dimension(s_size.width, 25));
		setJMenuBar(menuBar);
		
		JMenu mnMenu = new JMenu("Menu");
		menuBar.add(mnMenu);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnMenu.add(mntmExit);
		mntmExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("exiting");
				System.exit(0);
			}
		});
		
		getContentPane().setLayout(null);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setPreferredSize(new Dimension(s_size.width, s_size.height-35));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		leftPanel = new LeftPanel(300, s_size.height - 75);
		leftPanel.setBounds(5, 5, 300, s_size.height - 75);
		leftPanel.getOptionsPanel().setGame(this);
		contentPane.add(leftPanel);
		
		createRightPanel();
		

	}
	
	public void createRightPanel() {
		Dimension s_size = Toolkit.getDefaultToolkit().getScreenSize();
		
		ImgConf config = new ImgConf(leftPanel.getCellSize(), leftPanel.getLineWidth(), 0, 0);
		Dimension rp_size = new Dimension(s_size.width - 320, s_size.height - 75);
		int N = (rp_size.width - config.line_width) / config.cell_size;
		int M = (rp_size.height - config.line_width) / config.cell_size;
		if (N < 2) N = 2;
		if (M < 2) M = 2;
		model = new Model(N, M);
		
		if (rightPanel == null) {			
			rightPanel = new RightPanel();
			rightPanel.setBounds(310, 5, rp_size.width, rp_size.height);
			listener = new ImageMouseListener();
			rightPanel.addMouseListener(listener);
			rightPanel.addMouseMotionListener(listener);
			
			contentPane.add(rightPanel);
			rightPanel.init();
			
			leftPanel.getOptionsPanel().setRightPanel(rightPanel);
		} else {
			listener.setModel(model);
		}
		
		rightPanel.setConfig(config);
		rightPanel.addModel(model);
		listener.setModel(model);
		leftPanel.getOptionsPanel().setModel(model);
		leftPanel.getGamePanel().setModel(model);
		leftPanel.setModel(model);
		leftPanel.setRightPanel(rightPanel);
		model.setNotifications(leftPanel.getGamePanel());
	}
}
