package me.nfs;

import java.util.TimerTask;

public class MoveTask extends TimerTask{
	private final Model model;
	private final Model.Car car;
	private final Model.Direction d;
	private final Coord dest;
	
	public MoveTask(Model m, Model.Car c, Model.Direction d, Coord dest) {
		this.model = m;
		this.car = c;
		this.d = d;
		this.dest = dest;
	}
	@Override
	public void run() {
		model.move(car, d);
		if (car.coord.equals(dest)) {
			model.setWaiting(false);
		}
	}
}
