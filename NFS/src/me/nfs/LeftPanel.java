package me.nfs;

import java.awt.Dimension;

import javax.swing.JPanel;

public class LeftPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3503493479850778530L;
	private Mode mode;
	private Model model;
	private OptionsPanel options;
	private GamePanel game;
	private RightPanel right_panel;

	/**
	 * Create the panel.
	 */
	public LeftPanel(int w, int h) {
		mode = Mode.CONFIGURATION;
		
		setLayout(null);
		setPreferredSize(new Dimension(w,h));
		setSize(w, h);

		System.out.println("lp " + getSize().toString());
		
		options = new OptionsPanel(this);
		options.setBounds(0, 0, getWidth(), getHeight());
		options.setVisible(true);
		add(options);
		
		game = new GamePanel(this);
		game.setBounds(0, 0, getWidth(), getHeight());
		game.setVisible(false);
		add(game);
	}
	
	public void switchMode() {
		switch(mode) {
		case CONFIGURATION:
			options.setVisible(false);
			game.setVisible(true);
			model.putCars();
			model.setEditable(false);
			right_panel.repaint();
			mode = Mode.GAME;
			break;
		case GAME:
			game.setVisible(false);
			options.setVisible(true);
			model.reset();
			model.setEditable(true);
			right_panel.repaint();
			mode = Mode.CONFIGURATION;
			break;
		}
	}
	
	public enum Mode {
		CONFIGURATION, GAME;
	}
	
	public int getCellSize() {
		return options.getCellSize();
	}
	
	public int getLineWidth() {
		return options.getLineWidth();
	}
	
	public final OptionsPanel getOptionsPanel() {
		return options;
	}
	
	public void setRightPanel(RightPanel p) {
		right_panel = p;
	}
	
	public void setModel(Model m) {
		model = m;
	}
	
	public GamePanel getGamePanel() {
		return game;
	}
}
