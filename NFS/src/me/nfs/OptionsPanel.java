package me.nfs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class OptionsPanel extends JPanel {
	private JTextField fieldCellSize;
	private JTextField fieldLineWidth;
	private LeftPanel lpanel;
	private Model model;
	private RightPanel right_panel;
	private Game game;

	/**
	 * Create the panel.
	 */
	public OptionsPanel(LeftPanel lp) {
		lpanel = lp;
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblCellSize = new JLabel("Cell size");
		GridBagConstraints gbc_lblCellSize = new GridBagConstraints();
		gbc_lblCellSize.anchor = GridBagConstraints.EAST;
		gbc_lblCellSize.insets = new Insets(0, 0, 5, 5);
		gbc_lblCellSize.gridx = 0;
		gbc_lblCellSize.gridy = 0;
		add(lblCellSize, gbc_lblCellSize);
		
		fieldCellSize = new JTextField();
		fieldCellSize.setText("25");
		GridBagConstraints gbc_fieldCellSize = new GridBagConstraints();
		gbc_fieldCellSize.anchor = GridBagConstraints.WEST;
		gbc_fieldCellSize.insets = new Insets(0, 0, 5, 0);
		gbc_fieldCellSize.gridx = 1;
		gbc_fieldCellSize.gridy = 0;
		add(fieldCellSize, gbc_fieldCellSize);
		fieldCellSize.setColumns(10);
		
		JLabel lblLineWidth = new JLabel("Line width");
		GridBagConstraints gbc_lblLineWidth = new GridBagConstraints();
		gbc_lblLineWidth.anchor = GridBagConstraints.EAST;
		gbc_lblLineWidth.insets = new Insets(0, 0, 5, 5);
		gbc_lblLineWidth.gridx = 0;
		gbc_lblLineWidth.gridy = 1;
		add(lblLineWidth, gbc_lblLineWidth);
		
		fieldLineWidth = new JTextField();
		fieldLineWidth.setText("3");
		GridBagConstraints gbc_fieldLineWidth = new GridBagConstraints();
		gbc_fieldLineWidth.anchor = GridBagConstraints.WEST;
		gbc_fieldLineWidth.insets = new Insets(0, 0, 5, 0);
		gbc_fieldLineWidth.gridx = 1;
		gbc_fieldLineWidth.gridy = 1;
		add(fieldLineWidth, gbc_fieldLineWidth);
		fieldLineWidth.setColumns(10);
		
		JButton btnReset = new JButton("Reset");
		GridBagConstraints gbc_btnReset = new GridBagConstraints();
		gbc_btnReset.anchor = GridBagConstraints.WEST;
		gbc_btnReset.insets = new Insets(0, 0, 5, 5);
		gbc_btnReset.gridx = 0;
		gbc_btnReset.gridy = 3;
		add(btnReset, gbc_btnReset);
		btnReset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.clear();
				right_panel.repaint();
			}
		});
		
		JButton btnUpdate = new JButton("Update");
		GridBagConstraints gbc_btnUpdate = new GridBagConstraints();
		gbc_btnUpdate.insets = new Insets(0, 0, 5, 0);
		gbc_btnUpdate.anchor = GridBagConstraints.WEST;
		gbc_btnUpdate.gridx = 1;
		gbc_btnUpdate.gridy = 3;
		add(btnUpdate, gbc_btnUpdate);
		btnUpdate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				game.createRightPanel();
			}
		});
		
		JButton btnStartRace = new JButton("Start Race!");
		GridBagConstraints gbc_btnStartRace = new GridBagConstraints();
		gbc_btnStartRace.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnStartRace.insets = new Insets(0, 0, 0, 5);
		gbc_btnStartRace.gridx = 0;
		gbc_btnStartRace.gridy = 5;
		add(btnStartRace, gbc_btnStartRace);
		btnStartRace.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				lpanel.switchMode();
			}
		});
	}
	
	public int getCellSize() {
		int ret = 0;
		try {
			ret = Integer.parseInt(fieldCellSize.getText());
		} catch (NumberFormatException ex) {
			ret = 25;
		}
		return ret;
	}

	public int getLineWidth() {
		int ret = 0;
		try {
			ret = Integer.parseInt(fieldLineWidth.getText());
		} catch (NumberFormatException ex) {
			ret = 3;
		}
		return ret;
	}
	
	public void setModel(Model m) {
		model = m;
	}
	
	public void setRightPanel(RightPanel rp) {
		right_panel = rp;
	}
	
	public void setGame(Game g) {
		game = g;
	}
}
