package me.nfs;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class RightPanel extends JPanel implements Runnable{
	
	private static final long serialVersionUID = 533538694023615803L;
	private Thread t;
	private BufferedImage image;
	private Graphics image_graphics;
	private Model model;
	private ImgConf config;
	
	/**
	 * Create the panel.
	 */
	public RightPanel() {
		super();
	}
	
	public final ImgConf getConfig() {
		return config;
	}

	public void paint(Graphics g) {
		if (image == null) {
			image = (BufferedImage) createImage(getWidth(), getHeight());
		}
		image_graphics = image.getGraphics();
		image_graphics.clearRect(0, 0, getWidth(), getHeight());
		for (int i = 0; i < model.getN(); ++i) {
			for (int j = 0; j < model.getM(); ++j) {
				image_graphics.setColor(getColor(model.getCell(i, j)));
				image_graphics.fillRoundRect(i*config.cell_size, j*config.cell_size, 
											config.cell_size, config.cell_size, 
											config.arc_width, config.arc_height);
			}
		}
		
		// Сетка
		image_graphics.setColor(Color.DARK_GRAY);
		for (int i = 0; i <= model.getN(); ++i)
			image_graphics.fillRect(i*config.cell_size, 0, config.line_width, model.getM()*config.cell_size + config.line_width);
		for (int i = 0; i <= model.getM(); ++i)
			image_graphics.fillRect(0, i*config.cell_size, model.getN()*config.cell_size + config.line_width, config.line_width);
		
		// Players
		Coord p1 = model.getPlayerCoord(1);
		Coord p2 = model.getPlayerCoord(2);
		if (p1 != null) {
			image_graphics.setColor(Color.BLACK);
			image_graphics.fillRoundRect(p1.x*config.cell_size, p1.y*config.cell_size, 
					config.cell_size, config.cell_size, 
					config.arc_width, config.arc_height);
		}
		if (p2 != null) {
			image_graphics.setColor(Color.BLACK);
			image_graphics.fillRoundRect(p2.x*config.cell_size, p2.y*config.cell_size, 
					config.cell_size, config.cell_size, 
					config.arc_width, config.arc_height);
		}
		if (p1 != null) {
			image_graphics.setColor(Color.GREEN);
			image_graphics.fillRoundRect(p1.x*config.cell_size-3, p1.y*config.cell_size-3, 
					config.cell_size, config.cell_size, 
					config.arc_width, config.arc_height);
		}
		if (p2 != null) {
			image_graphics.setColor(Color.RED);
			image_graphics.fillRoundRect(p2.x*config.cell_size-3, p2.y*config.cell_size-3, 
					config.cell_size, config.cell_size, 
					config.arc_width, config.arc_height);
		}
        
		g.drawImage(image, 0, 0, this);
	}
	
	public Color getColor(Cell c) {
		if (c != null) {
			Color color = Color.WHITE;
			switch (c) {
			case EMPTY : color = Color.WHITE; break;
			case START : color = Color.YELLOW; break;
			case FINISH : color = Color.BLUE; break;
			case WALL : color = Color.BLACK; break;
			case STONE : color = Color.DARK_GRAY; break;
			case CAR : color = Color.GREEN; break;
			default : 
				System.err.println("getColor : unknown cell type");
			}
			return color;
		} else {
			System.err.println("getColor : cell == null");
			return Color.WHITE;
		}
	}
	
	public void update(Graphics g) {
		paint(g);
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		boolean running = true;
		while (running) {
			repaint();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ex) {
				running = false;
			}
		}
	}
	
	public void addModel(Model m) {
		model = m;
	}
	
	public void setConfig(ImgConf c) {
		config = c;
	}
	
	public void init() {
		t = new Thread(this);
		t.start();
	}
	
	public void interrupt() {
		t.interrupt();
	}
}
