package me.nfs;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.PriorityBlockingQueue;

public class Model {
	private int N;
	private int M;
	private Cell field[][];
	private Car player1;
	private Car player2;
	private Coord start1;
	private Coord start2;
	boolean editable;
	private int turn;
	private boolean waiting;
	private Timer t;
	private GamePanel notifications;
	private boolean finished;
	private List<Direction> path;

	public enum Direction {
		UP, DOWN, LEFT, RIGHT;
	}

	public class Car {
		public Coord coord;
		public int vx;
		public int vy;
		public Cell cell;

		public Car(int x, int y, int vx, int vy, Cell c) {
			this.coord = new Coord(x, y);
			this.vx = vx;
			this.vy = vy;
			this.cell = c;
		}
	}

	public Model(int n, int m) {
		turn = 0;
		editable = true;
		waiting = false;
		finished = false;
		path = new LinkedList<Direction>();
		N = n;
		M = m;
		field = new Cell[N + 1][M + 1];
		for (int i = 0; i < N + 1; ++i)
			for (int j = 0; j < M + 1; ++j)
				field[i][j] = Cell.EMPTY;
		for (int i = 1; i < M - 1; ++i) {
			field[0][i] = field[1][i] = Cell.START;
			field[N - 2][i] = field[N - 1][i] = Cell.FINISH;
		}
		for (int i = 0; i < N; ++i) {
			field[i][0] = Cell.WALL;
			field[i][M - 1] = Cell.WALL;
		}
	}

	public void putCars() {
		if (player1 == null) {
			field[1][M / 2] = Cell.CAR;
			player1 = new Car(1, M / 2, 0, 0, Cell.START);
		}
		if (player2 == null) {
			field[0][M / 2] = Cell.CAR;
			player2 = new Car(0, M / 2, 0, 0, Cell.START);
		}
		if (start1 == null)
			start1 = new Coord();
		if (start2 == null)
			start2 = new Coord();
		start1.setCoord(player1.coord);
		start2.setCoord(player2.coord);
	}

	public void invertCell(int i, int j) {
		if (editable) {
			if (i < N && j < M) {
				Cell cell = field[i][j];
				switch (cell) {
				case EMPTY:
					field[i][j] = Cell.STONE;
					break;
				case STONE:
					field[i][j] = Cell.EMPTY;
					break;
				case CAR: {
					Coord c = new Coord(i, j);
					if (player1 != null)
						if (player1.coord.equals(c))
							player1 = null;
					if (player2 != null)
						if (player2.coord.equals(c))
							player2 = null;
					field[i][j] = Cell.START;
					break;
				}
				case START: {
					boolean found = false;
					if (player1 == null) {
						found = true;
						player1 = new Car(i, j, 0, 0, Cell.START);
					}
					if (!found && player2 == null) {
						found = true;
						player2 = new Car(i, j, 0, 0, Cell.START);
					}
					if (found)
						field[i][j] = Cell.CAR;
					break;
				}
				default:
					break;
				}
			} else {
				System.err.println("invertCell : out of range i " + i + ", j "
						+ j);
			}
		} else {
			System.err.println("invertCell : model is not editable");
		}
	}

	public Car getCar(int x, int y) {
		Coord c = new Coord(x, y);
		if (c.equals(player1.coord))
			return player1;
		if (c.equals(player2.coord))
			return player2;
		return null;
	}
	
	public Coord getPlayerCoord(int num) {
		switch (num) {
		case 1: if (player1 != null) return new Coord(player1.coord);
		case 2: if (player2 != null) return new Coord(player2.coord);
		default: break;
		}
		return null;
	}

	public void invertCell(Coord c) {
		invertCell(c.x, c.y);
	}

	public void reset() {
		if (waiting) {
			t.cancel();
			waiting = false;
		}
		finished = false;
		turn = 0;
		field[player1.coord.x][player1.coord.y] = player1.cell;
		field[player2.coord.x][player2.coord.y] = player2.cell;
		player1.coord.setCoord(start1);
		player2.coord.setCoord(start2);
		field[player1.coord.x][player1.coord.y] = Cell.CAR;
		field[player2.coord.x][player2.coord.y] = Cell.CAR;
		player1.vx = player1.vy = 0;
		player2.vx = player2.vy = 0;
		player1.cell = Cell.START;
		player2.cell = Cell.START;
	}

	public void clear() {
		if (editable) {
			for (int i = 2; i < N - 2; ++i) {
				for (int j = 1; j < M - 1; ++j) {
					field[i][j] = Cell.EMPTY;
				}
			}
		} else {
			System.err.println("clear : model is not editable");
		}
	}

	public final Cell getCell(int i, int j) {
		if (i > N) {
			System.err.println("Model.getCell i: out of range");
			return Cell.EMPTY;
		}
		if (j > M) {
			System.err.println("Model.getCell j: out of range");
			return Cell.EMPTY;
		}
		return field[i][j];
	}

	public final Cell getCell(Coord c) {
		return getCell(c.x, c.y);
	}

	public int getN() {
		return N;
	}

	public int getM() {
		return M;
	}

	public void setEditable(boolean b) {
		editable = b;
	}

	public void move(int vx, int vy) {
		if (!waiting && !finished) {
			Car p = null;
			if (turn == 0)
				p = player1;
			else if (turn == 1)
				p = player2;

			final Car player = p;

			player.vx += vx;
			if (player.vx < 0)
				player.vx = 0;
			player.vy += vy;
			waiting = true;
			Coord dest = new Coord(player.coord.x + player.vx, player.coord.y
					+ player.vy);
			if (dest.x >= N) dest.x = N-1;
			System.out.println("moving from " + player.coord + " to " + dest);

			boolean cond = false;
			if (N <= dest.x || M <= dest.y || dest.x < 0 || dest.y < 0) {
				cond = true;
			} else if (field[dest.x][dest.y] == Cell.WALL
					|| field[dest.x][dest.y] == Cell.STONE
					|| field[dest.x][dest.y] == Cell.CAR) {
				cond = true;
			} else if (player.coord.equals(dest)) {
				cond = true;
			} else if (!findPath(player.coord, dest)) {
				cond = true;
			} else {
				
				t = new Timer();
				int delay = 0;
				for (Direction d : path) {
					t.schedule(new MoveTask(this, player, d, dest),
							delay++ * 1000);
				}
			}

			if (cond) {
				if (player.coord.equals(dest)) {
					waiting = false;
				}

				t = new Timer();
				for (int i = 0; i < player.vx; ++i) {
					t.schedule(new MoveTask(this, player, Direction.UP, dest),
							i * 1000);
				}
				if (player.vy >= 0) {
					for (int i = 0; i < player.vy; ++i) {
						t.schedule(new MoveTask(this, player, Direction.RIGHT,
								dest), player.vx * 1000 + i * 1000);
					}
				} else {
					for (int i = 0; i > player.vy; --i) {
						t.schedule(new MoveTask(this, player, Direction.LEFT,
								dest), player.vx * 1000 - i * 1000);
					}
				}
			}

			++turn;
			turn %= 2;
		}
	}

	public void move(Car c, Direction d) {
		switch (d) {
		case DOWN:
		case UP:
			moveX(c);
			break;
		case LEFT:
		case RIGHT:
			moveY(c, d);
			break;
		}
	}

	private void moveX(Car player) {
		field[player.coord.x][player.coord.y] = player.cell;
		++player.coord.x;
		player.cell = field[player.coord.x][player.coord.y];
		field[player.coord.x][player.coord.y] = Cell.CAR;
		check(player);
	}

	private void moveY(Car player, Direction d) {
		field[player.coord.x][player.coord.y] = player.cell;
		switch (d) {
		case LEFT:
			--player.coord.y;
			break;
		case RIGHT:
			++player.coord.y;
			break;
		default:
			break;
		}
		player.cell = field[player.coord.x][player.coord.y];
		field[player.coord.x][player.coord.y] = Cell.CAR;
		check(player);
	}

	private void check(Car player) {
		if (player1.cell == Cell.STONE || player1.cell == Cell.WALL
				|| player1.cell == Cell.CAR) {
			finished = true;
			notifications.setBreakVisible(true, 1);
			try {
				t.cancel();
			} catch (IllegalStateException ex) {
			}
			t = new Timer();
			t.schedule(new TimerTask() {
				@Override
				public void run() {
					notifications.setBreakVisible(false, 1);
				}
			}, 10000);
		} else if (player2.cell == Cell.STONE || player2.cell == Cell.WALL
				|| player2.cell == Cell.CAR) {
			finished = true;
			notifications.setBreakVisible(true, 2);
			try {
				t.cancel();
			} catch (IllegalStateException ex) {
			}
			t = new Timer();
			t.schedule(new TimerTask() {
				@Override
				public void run() {
					notifications.setBreakVisible(false, 2);
				}
			}, 10000);
		} else if (player1.cell == Cell.FINISH) {
			finished = true;
			notifications.setWinVisible(true, 1);
			try {
				t.cancel();
			} catch (IllegalStateException ex) {
			}
			t = new Timer();
			t.schedule(new TimerTask() {
				@Override
				public void run() {
					notifications.setWinVisible(false, 1);
				}
			}, 3000);
		} else if (player2.cell == Cell.FINISH) {
			finished = true;
			notifications.setWinVisible(true, 2);
			try {
				t.cancel();
			} catch (IllegalStateException ex) {
			}
			t = new Timer();
			t.schedule(new TimerTask() {
				@Override
				public void run() {
					notifications.setWinVisible(false, 2);
				}
			}, 3000);
		}
	}

	public boolean isWaiting() {
		return waiting;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setWaiting(boolean b) {
		waiting = b;
	}

	public void setNotifications(GamePanel p) {
		notifications = p;
	}

	public int getPlayerNUM() {
		switch (turn) {
		case 0:
			return 1;
		case 1:
			return 2;
		default:
			return 0;
		}
	}

	public int getPlayerVX() {
		switch (turn) {
		case 0:
			return player1.vx;
		case 1:
			return player2.vx;
		default:
			return 0;
		}
	}

	public int getPlayerVY() {
		switch (turn) {
		case 0:
			return player1.vy;
		case 1:
			return player2.vy;
		default:
			return 0;
		}
	}

	public int getTurn() {
		return turn;
	}

	public boolean findPath(final Coord start, final Coord finish) {
		System.out.println("building path");
		path.clear();
		boolean existPath = true;
		PriorityQueue<Coord> queue = new PriorityQueue<Coord>(100,
				new Comparator<Coord>() {
					@Override
					public int compare(Coord o1, Coord o2) {
						double l1 = o1.length(finish);
						double l2 = o2.length(finish);
						if (l1 == l2) {
							return 0;
						} else if (l1 > l2) {
							return 1;
						} else {
							return -1;
						}
					}
				});

		int n = Math.abs(start.x - finish.x) + 1;
		int m = Math.abs(start.y - finish.y) + 1;
		Direction matrix[][] = new Direction[n][m];
		if (start.y == finish.y) {
			System.out.println("trivial: y = y");
			for (int i = start.x; i < finish.x; ++i) {
				path.add(Direction.UP);
				if (field[i][start.y] == Cell.STONE
						|| field[i][start.y] == Cell.WALL
						|| field[i][start.y] == Cell.CAR) {
					existPath = false;
					break;
				}
			}
		} else if (start.x == finish.x) {
			System.out.println("trivial: x = x");
			if (finish.y > start.y) {
				for (int i = start.y; i < finish.y; ++i) {
					path.add(Direction.RIGHT);
					if (field[i][start.y] == Cell.STONE
							|| field[i][start.y] == Cell.WALL
							|| field[i][start.y] == Cell.CAR) {
						existPath = false;
						break;
					}
				}
			} else {
				for (int i = start.y; i > finish.y; --i) {
					path.add(Direction.LEFT);
					if (field[i][start.y] == Cell.STONE
							|| field[i][start.y] == Cell.WALL) {
						existPath = false;
						break;
					}
				}
			}
		} else {
			System.out.println("non-trivial");
			if (start.y < finish.y) {
				System.out.println("trivial: start.y < finish.y");
				queue.add(start);
				while (!queue.isEmpty()) {
					Coord c = queue.poll();
					System.out.println("picked : " + c);
					if (c.x + 1 <= finish.x) {
						if (finish.equals(new Coord(c.x + 1, c.y))) {
							matrix[n - 1][m - 1] = Direction.UP;
							break;
						} else if (field[c.x + 1][c.y] != Cell.STONE
								&& field[c.x + 1][c.y] != Cell.WALL
								&& field[c.x + 1][c.y] != Cell.CAR) {
							if (matrix[c.x + 1 - start.x][c.y - start.y] == null) {
								System.out.println("add to queue " + (c.x + 1)
										+ " " + c.y);
								queue.add(new Coord(c.x + 1, c.y));
								matrix[c.x + 1 - start.x][c.y - start.y] = Direction.UP;
							}
						}
					}
					if (c.y + 1 <= finish.y) {
						if (finish.equals(new Coord(c.x, c.y + 1))) {
							matrix[n - 1][m - 1] = Direction.RIGHT;
							break;
						} else if (field[c.x][c.y + 1] != Cell.STONE
								&& field[c.x][c.y + 1] != Cell.WALL
								&& field[c.x][c.y + 1] != Cell.CAR) {
							if (matrix[c.x - start.x][c.y + 1 - start.y] == null) {
								System.out.println("add to queue " + c.x + " "
										+ (c.y + 1));
								queue.add(new Coord(c.x, c.y + 1));
								matrix[c.x - start.x][c.y + 1 - start.y] = Direction.RIGHT;
							}
						}
					}
				}
				if (matrix[n - 1][m - 1] == null) {
					existPath = false;
				} else {
					System.out.println("fill path list");
					for (int i = 0; i < n; ++i) {
						for (int j = 0; j < m; ++j) {
							System.out.print(matrix[i][j] + " ");
						}
						System.out.println("");
					}
					Coord c = new Coord(n - 1, m - 1);
					while (!c.equals(new Coord(0, 0))) {
						path.add(0, matrix[c.x][c.y]);
						switch (matrix[c.x][c.y]) {
						case RIGHT:
							--c.y;
							break;
						case UP:
							--c.x;
							break;
						default:
							break;
						}
					}
				}

			} else {
				System.out.println("trivial: start.y > finish.y");
				queue.add(start);
				while (!queue.isEmpty()) {
					Coord c = queue.poll();
					System.out.println("picked : " + c);
					if (c.x + 1 <= finish.x) {
						if (finish.equals(new Coord(c.x + 1, c.y))) {
							matrix[n - 1][0] = Direction.UP;
							break;
						} else if (field[c.x + 1][c.y] != Cell.STONE
								&& field[c.x + 1][c.y] != Cell.WALL
								&& field[c.x + 1][c.y] != Cell.CAR) {
							// TODO:
							System.out.println((n - 1 - (start.x - (c.x + 1))) + " " + (m - 1
									- (start.y - c.y)));
							if (matrix[(c.x + 1) - start.x][m - 1
									- (start.y - c.y)] == null) {
								queue.add(new Coord(c.x + 1, c.y));
								matrix[(c.x + 1) - start.x][m - 1
										- (start.y - c.y)] = Direction.UP;
							}
						}
					}
					if (c.y - 1 >= finish.y) {
						if (finish.equals(new Coord(c.x, c.y - 1))) {
							matrix[n - 1][0] = Direction.LEFT;
							break;
						} else if (field[c.x][c.y - 1] != Cell.STONE
								&& field[c.x][c.y - 1] != Cell.WALL
								&& field[c.x][c.y - 1] != Cell.CAR) {
							if (matrix[c.x - start.x][m - 1
									- (start.y - (c.y - 1))] == null) {
								queue.add(new Coord(c.x, c.y - 1));
								matrix[c.x - start.x][m - 1
										- (start.y - (c.y - 1))] = Direction.LEFT;
							}
						}
					}
				}
				if (matrix[n - 1][0] == null) {
					existPath = false;
				} else {
					System.out.println("fill path list");
					for (int i = 0; i < n; ++i) {
						for (int j = 0; j < m; ++j) {
							System.out.print(matrix[i][j] + " ");
						}
						System.out.println("");
					}
					Coord c = new Coord(n - 1, 0);
					while (!c.equals(new Coord(0, m - 1))) {
						path.add(0, matrix[c.x][c.y]);
						switch (matrix[c.x][c.y]) {
						case LEFT:
							++c.y;
							break;
						case UP:
							--c.x;
							break;
						default:
							break;
						}
					}
				}
			}
		}

		return existPath;
	}
}
